export class Purchase
{
    public customerName: string;
    public offeringID: number;
    public quantity: number;
}