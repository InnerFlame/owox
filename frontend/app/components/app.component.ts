import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {Http, Response, Headers, RequestOptions} from '@angular/http';

// services
import {PurchaseService} from '../services/app.services.purchase';
// models
import {PurchaseModel} from '../models/app.models.purchase';

@Component({
    selector: 'my-app',
    template: `

        <div class="form-group">
            <label>customerName</label>
            <input class="form-control" name="username" [(ngModel)]="purchase.customerName" />
        </div>
        <div class="form-group">
            <label>offeringID</label>
            <input class="form-control" type="number" name="age" [(ngModel)]="purchase.offeringID" />
        </div>
         <div class="form-group">
            <label>quantity</label>
            <input class="form-control" type="number" name="age" [(ngModel)]="purchase.quantity" />
        </div>
        <div class="form-group">
            <button class="btn btn-default" (click)="create(purchase)">Create</button>
        </div>

        <ul>
            <li *ngFor="let purchase of purchases">
                <p>customerName: {{purchase.customerName}}</p>
                <p>offeringID: {{purchase.offeringID}}</p>
                <p>quantity: {{purchase.quantity}}</p>
            </li>
        </ul>


    `,
    providers: [PurchaseService]
})
export class AppComponent {

    public purchase: {} = {};
    public purchases: PurchaseModel[] = [];

    constructor(
        private purchaseService: PurchaseService
    ){}


    ngOnInit() {
        this.purchaseService.getData().subscribe((resp: Response) => {
            this.purchases = resp.json();
        });
    }

    create(purchase) {
        this.purchaseService.setData(purchase).subscribe((resp: Response) => {
           console.log(resp);
        });
    }

}